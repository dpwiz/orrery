module Global.Resource.Model where

import RIO.Local

import Resource.Buffer qualified as Buffer
import Render.Lit.Colored.Model qualified as ColoredLit
-- import Render.Lit.Material.Collect (LoadedModel)
import Render.Unlit.Colored.Model qualified as ColoredUnlit
import Render.Unlit.Textured.Model qualified as TexturedUnlit

data Collection = Collection
  { bbWire     :: ColoredUnlit.Model 'Buffer.Staged
  , icosphere1 :: ColoredLit.Model 'Buffer.Staged
  , icosphere4 :: ColoredLit.Model 'Buffer.Staged
  , quadUV     :: TexturedUnlit.Model 'Buffer.Staged

  , zeroTransform :: Buffer.Allocated 'Buffer.Staged Transform
  }

module Stage.Main.Scene where

import RIO.Local

import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker
import Geomancy.Transform qualified as Transform
import Geomancy.Vec3 qualified as Vec3
import Geomancy.Vec4 qualified as Vec4
import Render.DescSets.Set0 (Scene(..), emptyScene)
import Render.DescSets.Sun (Sun(..))
import RIO.Vector.Storable qualified as Storable

-- import Global.Resource.CubeMap qualified as CubeMap
import Stage.Main.World qualified as World

type CameraProcess = Worker.Timed CameraControls Camera.View

data CameraControls = CameraControls
  { ccAzimuth  :: Float
  , ccAscent   :: Float
  , ccDistance :: Float
  , ccPan      :: Vec3
  }

data CameraEvent
  = CameraAzimuth Float
  | CameraAscent Float
  | CameraDistance Float
  | CameraPan Vec3

spawnCamera :: MonadUnliftIO m => m CameraProcess
spawnCamera =
  Worker.spawnTimed
    True
    (Left interval)
    mkState
    mkView
    initialConfig
  where
    interval = 10_000
    dt = 1 / fromIntegral interval

    mkState _config =
      pure
        ( Camera.mkViewOrbital_ initialState
        , initialState
        )

    mkView Camera.ViewOrbitalInput{..} CameraControls{..} = do
      let
        adjust = or
          [ ccAzimuth /= 0
          , ccAscent /= 0
          , ccDistance /= 0
          , ccPan /= 0
          ]
        state' = Camera.ViewOrbitalInput
          { orbitAzimuth  = orbitAzimuth + ccAzimuth * dt
          , orbitAscent   = trimAscent $ orbitAscent + ccAscent * dt
          , orbitDistance = trimDistance $ orbitDistance + zoomSpeed * ccDistance * dt
          , orbitTarget   = orbitTarget + ccPan Vec3.^* dt
          , ..
          }
      when (ccPan /= 0) $
        traceShowM ccPan
      pure
        ( if adjust then
            Just $ Camera.mkViewOrbital_ state'
          else
            Nothing
        , state'
        )

    trimAscent = trim (-τ/4 + eta) (τ/4 - eta)
      where
        eta = 1/64

    trimDistance =
      trim
        (6371 * World.SCALE + 2) -- XXX: planet radius + clip near
        (CLIP_FAR * 0.5) -- XXX: half clip far

    zoomSpeed = CLIP_FAR

    initialState = Camera.ViewOrbitalInput
      { orbitAzimuth  = 0
      , orbitAscent   = 0
      , orbitDistance = CLIP_FAR * World.SCALE
      , orbitScale    = 1
      , orbitTarget   = 0
      }

    initialConfig = CameraControls
      { ccAzimuth  = 0
      , ccAscent   = 0
      , ccDistance = 0
      , ccPan      = 0
      }

-- Engine hardcode
pattern CLIP_FAR :: (Eq a, Num a) => a
pattern CLIP_FAR = 16384

type Process = Worker.Merge Scene

mkScene :: Camera.Projection -> Camera.View -> Scene
mkScene Camera.Projection{..} Camera.View{..} =
  emptyScene
    { sceneProjection    = projectionPerspective
    , sceneInvProjection = Transform.inverse projectionPerspective -- FIXME: move to cell output

    , sceneView          = viewTransform
    , sceneInvView       = viewTransformInv
    , sceneViewPos       = viewPosition
    , sceneViewDir       = viewDirection

    , sceneEnvCube       = -1 -- CubeMap.milkyway CubeMap.indices
    , sceneNumLights     = fromIntegral $ Storable.length staticLights
    }

mkSceneUi :: Camera.Projection -> Scene
mkSceneUi Camera.Projection{..} =
  emptyScene
    { sceneProjection    = projectionOrthoUI
    , sceneInvProjection = Transform.inverse projectionOrthoUI -- FIXME: move to cell output
    , sceneView          = mempty
    , sceneInvView       = mempty
    }

staticLights :: Storable.Vector Sun
staticLights =
  Storable.fromList
    [ mkSun (τ*1/3) (τ/9) 1.0 Nothing -- TODO: (Just 0)
    ]

-- XXX: copypasta from keid-snap:Stage.Main.World.Scene
mkSun :: Float -> Float -> Vec4 -> Maybe Natural -> Sun
mkSun azimuth inclination color mshadow = Sun
  { sunViewProjection = mconcat vp
  , sunShadow         = vec4 0 0 (maybe (-1) fromIntegral mshadow) size
  , sunPosition       = Vec4.fromVec3 position 0
  , sunDirection      = Vec4.fromVec3 direction 0
  , sunColor          = color
  }
  where
    size = 2 * 6371 * World.SCALE
    depthRange = 16_000 * World.SCALE
    distance = depthRange / 2

    -- XXX: copypasta from playgroun4:Stage.Example.World.Sun

    vp =
      [ Transform.rotateY (-azimuth)
      , Transform.rotateX (-inclination)

      , Transform.translate 0 0 distance

      -- XXX: some area beyond the near plane receives light, but not shadows
      , Transform.scale3
          (1 / size)
          (1 / size)
          (1 / depthRange)
      ]

    position = Transform.apply (vec3 0 0 distance) rotation

    direction = Transform.apply (vec3 0 0 $ -1) rotation

    rotation = mconcat
      [ Transform.rotateX inclination
      , Transform.rotateY azimuth
      ]

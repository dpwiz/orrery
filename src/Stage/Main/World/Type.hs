{-# LANGUAGE TemplateHaskell #-}

module Stage.Main.World.Type
  ( World
  , initWorld
  ) where

import Prelude
import Apecs.STM.Prelude

-- import Stage.Main.World.Orbiter qualified as Orbiter
import Stage.Main.World.Body qualified as Body
import Stage.Main.World.Wire qualified as Wire

makeWorld "World"
  [ ''Wire.Wire

  , ''Body.Body

  -- , ''Render.Instance
  -- , ''Render.TailInstances

  -- , ''Racer.Position
  ]

  -- [ ''Orbiter.Orbiter
  -- , ''Orbiter.MeanAnomaly
  -- , ''Orbiter.Motion
  -- , ''Orbiter.Trajectory

  -- , ''Orbiter.Direction

  --   -- XXX: move to Vessel?
  -- , ''Orbiter.Orientation
  -- , ''Orbiter.Thrust

  -- , ''Orbiter.Player
  -- , ''Orbiter.Target
  -- , ''Orbiter.Pod
  -- , ''Orbiter.Launch

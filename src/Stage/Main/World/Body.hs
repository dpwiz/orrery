module Stage.Main.World.Body
  ( Body(..)
  , Vertex
  , Model
  ) where

-- import RIO.Local

import Apecs.STM.Prelude qualified as Apecs
-- import Geomancy (Vec3)
import Geomancy.Vec3 qualified as Vec3
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model
import Render.Lit.Colored.Model qualified as LitColored

data Body = Body
  { model :: Model
  , inst  :: LitColored.InstanceAttrs
  }

instance Apecs.Component Body where
  type Storage Body = Apecs.Map Body

type Vertex = Model.Vertex Vec3.Packed LitColored.VertexAttrs

type Model = Model.Indexed 'Buffer.Staged Vec3.Packed LitColored.VertexAttrs

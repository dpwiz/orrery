module Stage.Main.UI
  ( UI(..)
  , spawn

  , postMessage_
  -- , postMessageRare
  , postMessage

  , Observer(..)
  , newObserver
  , observe
  , readObserved
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Engine.Types qualified as Engine
import Engine.UI.Layout qualified as Layout
import Engine.UI.Message qualified as Message
import Engine.Vulkan.Types (HasVulkan)
import Engine.Worker qualified as Worker
-- import Formatting qualified as F
-- import Physics.Orbit qualified as Orbit
import Resource.Buffer qualified as Buffer
import Resource.Combined.Textures qualified as CombinedTextures
import RIO.List (unzip3)
import RIO.Vector.Storable qualified as Storable
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Global.Resource.Assets (Assets)
import Global.Resource.Assets qualified as Assets
import Global.Resource.Combined qualified as Combined
import Global.Resource.Font qualified as GlobalFont
-- import Stage.Main.World qualified as World
-- import Stage.Main.World.Orbiter qualified as Orbiter

data UI = UI
  { messageLines   :: [Worker.Var (Float, Vec4, Text)]
  , messageWorkers :: [Message.Process]
  , messageBgP     :: Worker.Merge (Storable.Vector Transform)

  , topMessageBgP  :: Worker.Merge (Storable.Vector Transform)
  -- , playerOrbiter  :: World.UIProbe
  -- , playerOrbiterP :: [Message.Process]
  }

-- data MkMessage where
--   MkMessage
--     :: Worker.Var a
--     -> (a -> Text)
--     -> (Message.Input -> Message.Input)
--     -> MkMessage

spawn
  :: Assets
  -> Layout.BoxProcess
  -> Engine.StageRIO env (Resource.ReleaseKey, UI)
spawn assets screenP = do
  -- Top HUD box

  (topMessageBoxKey, topMessageBoxP) <- Worker.registered $
    Layout.fitPlaceAbs Layout.CenterTop (vec2 480 42) screenP

  -- messagePadding <- Worker.newVar 8
  -- (topMessagePaddedKey, topMessagePaddedP) <- Worker.registered $
  --   Layout.padAbs topMessageBoxP messagePadding

  -- HUD probe

  -- uiPlayerOrientation <- Worker.newVar Nothing
  -- uiPlayerOrbit <- Worker.newVar Nothing
  -- uiTarget <- Worker.newVar Nothing

  -- let
  --   mkPlayerText (Orbiter.Player{playerPods}, orientation, mThrust) =
  --     F.sformat
  --       ( "Attachments: " <<< F.shown <<< " | " <<<
  --         "Orientation: " <<< F.shown <<< " | " <<<
  --         "Thrust: " <<< F.maybed "(offline)" formatThrust
  --         -- TODO: fuel
  --       )
  --       playerPods
  --       orientation
  --       mThrust

  --   formatThrust =
  --     F.accessed
  --       (\(Orbiter.Thrust t) -> t)
  --       (F.fixed 1)

  --   mkOrbiterText Orbiter.Orbiter{orbit=Orbit.Orbit{..}, meanMotion} =
  --     F.sformat
  --       ( "Periapsis: " <<< F.fixed 1 <<< " km " <<<
  --         formatPeriSpec <<< " " <<<
  --         formatInclination <<< " " <<<
  --         "Period: " <<< F.fixed 1 <<< " h"
  --       )
  --       (periapsis `numIn` [si| km |])
  --       (periapsisSpecifier, eccentricity)
  --       inclinationSpecifier
  --       ((turn |/| meanMotion) `numIn` [si| h |])
  --     where
  --       formatPeriSpec = F.later \case
  --         (Orbit.Circular, _zeroes) ->
  --           "Circular"
  --         (Orbit.Eccentric{argumentOfPeriapsis}, ecc) ->
  --           F.bformat
  --             ("Eccentric " <<< F.fixed 3 <<< " " <<< F.fixed 1 <<< "°")
  --             ecc
  --             (argumentOfPeriapsis `numIn` [si| deg |])

  --       formatInclination = F.later \case
  --         Orbit.NonInclined ->
  --           "Non-inclined"
  --         Orbit.Inclined{..} ->
  --           F.bformat
  --             ( "Inclined " <<< F.fixed 2 <<< "°, " <<<
  --               "AN lon.: " <<< F.fixed 2 <<< "°"
  --             )
  --             (inclination `numIn` [si| deg |])
  --             (longitudeOfAscendingNode `numIn` [si| deg |])

  --   mkTargetText (Orbiter.Target{..}, _orbiter) =
  --     F.sformat
  --       ("Target: " <<< F.fixed 1 <<< " km @ " <<< F.fixed 1 <<< " km/s")
  --       (targetDistance `numIn` [si| km |])
  --       (targetVelocity `numIn` [si| km/s |])

  -- (hudKeys, playerOrbiterP) <- spawnMessages topMessagePaddedP
  --   [ MkMessage
  --       uiPlayerOrientation
  --       (maybe mempty mkPlayerText)
  --       ( \m -> m
  --           { Message.inputOrigin = Layout.LeftTop
  --           }
  --       )
  --   , MkMessage
  --       uiPlayerOrbit
  --       (maybe mempty mkOrbiterText)
  --       ( \m -> m
  --           { Message.inputOrigin = Layout.LeftMiddle
  --           }
  --       )
  --   , MkMessage
  --       uiTarget
  --       (maybe mempty mkTargetText)
  --       ( \m -> m
  --           { Message.inputOrigin = Layout.LeftBottom
  --           }
  --       )
  --   ]

  (topMessageBgKey, topMessageBgP) <- Worker.registered $
    Worker.spawnMerge1 (Storable.singleton . Layout.boxTransformAbs) topMessageBoxP

  -- Message log
  (messageBoxKey, messageBoxP) <- Worker.registered $
    Layout.fitPlaceAbs
      Layout.CenterBottom
      (vec2 1024 $ sum lineSizes)
      screenP

  lineBoxes <- Layout.splitsRelStatic Layout.sharePadsV messageBoxP lineSizes
  lineBoxesKey <- Worker.registerCollection lineBoxes

  messageLines <- for lineSizes \lineSize ->
    Worker.newVar (lineSize, 1, "")

  messages <- for (zip lineBoxes messageLines) \(lineBoxP, messageVar) ->
    Message.spawnFromR lineBoxP messageVar \(size, color, text) ->
      messageLine size color text
  let (inputKeys, messageKeys, messageWorkers) = unzip3 messages

  (messageBgKey, messageBgP) <- Worker.registered $
    Worker.spawnMerge1 (Storable.singleton . Layout.boxTransformAbs) messageBoxP

  key <- Resource.register $ traverse_ Resource.release $ mconcat
    [ [ topMessageBoxKey
      , topMessageBgKey
      -- , topMessagePaddedKey

      , messageBoxKey
      , lineBoxesKey
      , messageBgKey
      ]
    -- , hudKeys
    , inputKeys
    , messageKeys
    ]

  -- let playerOrbiter = World.UIProbe{uiMessageLog=messageLines, ..}

  pure (key, UI{..})
  where

    -- spawnMessages
    --   :: Layout.BoxProcess
    --   -> [MkMessage]
    --   -> Engine.StageRIO env ([Resource.ReleaseKey], [Message.Process])
    -- spawnMessages boxP mkMessages = do
    --   spawned <- for mkMessages \(MkMessage var mkText withMessage) ->
    --     Message.spawnFromR boxP var \probe ->
    --       withMessage (messageLine 16 1 $ mkText probe)
    --   let (inputKeys, messageKeys, workers) = unzip3 spawned
    --   pure (inputKeys <> messageKeys, workers)

    messageLine size color text = Message.Input
      { inputText         = text
      , inputOrigin       = Layout.CenterBottom
      , inputSize         = size
      , inputColor        = color
      , inputFont         = GlobalFont.small $ Assets.aFonts assets
      , inputFontId       = GlobalFont.small $ CombinedTextures.fonts Combined.indices
      , inputOutline      = vec4 0 0 0 1
      , inputOutlineWidth = 2/32
      , inputSmoothing    = 4/32
      }

lineSizes :: [Float]
lineSizes = replicate 5 22

numLines :: Int
numLines = length lineSizes

-- * Updates

type MessageLog = [Worker.Var MessageLine]
type MessageLine = (Float, Vec4, Text)

postMessage :: MessageLog -> Float -> Vec4 -> Text -> STM ()
postMessage lineVars size color text = do
  current <- traverse (fmap Worker.vData . readTVar) lineVars
  let new = take numLines $ (size, color, text) : map fade current
  for_ (zip lineVars new) \(var, input) ->
    Worker.pushInputSTM var (const input)
  where
    fade (s, c, t) = (max 16 $ s - 1, c * 0.9, t)

-- postMessageRare :: UI -> GameStuff.Rarity -> Text -> STM ()
-- postMessageRare ui rarity text =
--   postMessage ui (fromIntegral $ 16 + fromEnum rarity) (rareColor rarity) text

postMessage_ :: MessageLog -> Text -> STM ()
postMessage_ lineVars = postMessage lineVars 16 0.9

-- rareColor :: GameStuff.Rarity -> Vec4
-- rareColor = \case
--   GameStuff.Common    -> rgbI 202 202 202
--   GameStuff.Uncommon  -> rgbI 128 207  63
--   GameStuff.Rare      -> rgbI  47 213   1
--   GameStuff.Epic      -> rgbI 189  63 250
--   GameStuff.Legendary -> rgbI 253 174  83
--   GameStuff.Mythic    -> rgbI 252 221 121
--   where
--     rgbI r g b = vec4 (r/255) (g/255) (b/255) 1

-- * Output

data Observer = Observer
  { messageLog   :: [Message.Observer]
  , messageLogBg :: BgObserver
  }

type BgBuffer = Buffer.Allocated 'Buffer.Coherent Transform
type BgObserver = Worker.ObserverIO BgBuffer

newObserver :: UI -> ResourceT (Engine.StageRIO st) Observer
newObserver UI{..} = do
  -- topMessages <- traverse (const $ Message.newObserver 256) playerOrbiterP
  -- topMessagesBg <- newBgObserver

  messageLog <- traverse (const $ Message.newObserver 256) messageWorkers
  messageLogBg <- newBgObserver

  pure Observer{..}

newBgObserver :: ResourceT (Engine.StageRIO st) BgObserver
newBgObserver = do
  context <- ask

  (_transient, initialData) <-
    Buffer.allocateCoherent
      context
      Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
      1
      mempty

  Worker.newObserverIO initialData

observe
  :: ( HasVulkan env
     )
  => UI
  -> Observer
  -> RIO env ()
observe UI{..} Observer{..} = do
  context <- ask

  -- traverse_ (uncurry Message.observe) $
  --   zip playerOrbiterP topMessages
  -- Message.observe topMessageP topMessages
  -- Worker.observeIO_ topMessageBgP topMessagesBg $
  --   Buffer.updateCoherentResize_ context

  traverse_ (uncurry Message.observe) $
    zip messageWorkers messageLog
  Worker.observeIO_ messageBgP messageLogBg $
    Buffer.updateCoherentResize_ context

readObserved
  :: MonadUnliftIO m
  => Observer
  -> m ([Message.Buffer], BgBuffer)
readObserved Observer{..} = (,)
  <$> traverse Worker.readObservedIO messageLog
  -- <*> Worker.readObservedIO topMessagesBg
  <*> Worker.readObservedIO messageLogBg

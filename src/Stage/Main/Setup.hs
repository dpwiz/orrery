{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Setup
  ( stackStage
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Engine.Camera qualified as Camera
import Engine.Events qualified as Events
-- import Engine.Sound.Device qualified as SoundDevice
-- import Engine.Sound.Source qualified as SoundSource
import Engine.Types (StackStage(..), StageRIO)
import Engine.Types qualified as Engine
import Engine.UI.Layout qualified as Layout
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.DescSets.Sun qualified as Sun
-- import Render.Samplers qualified as Samplers
import Render.ShadowMap.RenderPass qualified as ShadowPass
import Resource.Buffer qualified as Buffer
-- import Resource.Combined.Textures qualified as CombinedTextures
-- import Resource.CommandBuffer (withPools)
-- import Resource.Font qualified as Font
import Resource.Image qualified as Image
-- import Resource.Opus qualified as Opus
-- import Resource.Texture qualified as Texture
-- import Resource.Texture.Ktx1 qualified as Ktx1
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Global.Render (RenderPasses, Pipelines)
import Global.Render qualified as Render
import Global.Resource.Assets (Assets)
import Global.Resource.Assets qualified as Assets
-- import Global.Resource.Combined qualified as Combined
-- import Global.Resource.CubeMap qualified as CubeMap
-- import Global.Resource.Font qualified as GlobalFont
-- import Global.Resource.Sound qualified as Sound
-- import Global.Resource.Texture qualified as Textures
import Stage.Main.Event.Key qualified as Key
import Stage.Main.Event.Sink (handleEvent)
import Stage.Main.Render qualified as Render
-- import Global.Resource.Model qualified as Model
import Stage.Main.Scene qualified as Scene
import Stage.Main.Types (FrameResources(..), RunState(..), Stage)
import Stage.Main.UI qualified as UI
import Stage.Main.World qualified as World
import Stage.Main.World.System qualified as WorldSystem

-- loadAction :: (Text -> Engine.StageSetupRIO ()) -> Engine.StageSetupRIO Assets
-- loadAction update =
--   withPools \pools -> do
--     -- update "Loading sounds"
--     -- (soundDeviceKey, soundDevice) <- SoundDevice.allocate
--     -- (soundsKey, !sounds) <- SoundSource.allocateCollectionWith (Opus.load soundDevice) Sound.configs
--     -- SoundSource.play (Sound.bg sounds)
--     -- let
--     --   update msg = do
--     --     SoundSource.play (Sound.detect sounds)
--     --     update' msg

--     -- update "Loading fonts"
--     -- (fontKey, fonts) <- Font.allocateCollection pools GlobalFont.configs

--     -- update "Loading textures"
--     -- (textureKey, textures) <- Texture.allocateCollectionWith
--     --   (Ktx1.load pools)
--     --   Textures.sources

--     -- update "Loading cubemaps"
--     -- (cubemapsKey, cubemaps) <- Texture.allocateCollectionWith
--     --   (Ktx1.load pools)
--     --   CubeMap.sources

--     -- update "Loading models"
--     -- (modelsKey, models) <- Model.allocateCollection pools

--     update "Done"

--     key <- Resource.register $
--       traverse_ @[] Resource.release
--         [ fontKey
--         , textureKey
--         , cubemapsKey
--         , modelsKey
--         , soundsKey
--         , soundDeviceKey
--         ]

--     pure
--       ( key
--       , fmap Font.container fonts
--       , CombinedTextures.Collection
--           { textures = textures
--           , fonts    = fmap Font.texture fonts
--           }
--       , cubemaps
--       , models
--       , sounds
--       )

stackStage :: Assets -> StackStage
stackStage = StackStage . stage

stage :: Assets -> Stage
stage assets = Engine.Stage
  { sTitle = "Main"

  , sAllocateP  = Render.allocatePipelines
  , sInitialRS  = initialRunState assets
  , sInitialRR  = initialFrameResources assets
  , sBeforeLoop = beforeLoop

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands

  , sAfterLoop = afterLoop
  }
  where
    -- allocatePipelines swapchain rps = do
    --   (_, samplers) <- Samplers.allocate swapchain
    --   let sceneBinds = Set0.mkBindings samplers Combined.sources CubeMap.sources 1
    --   Basic.allocatePipelines sceneBinds swapchain rps

    beforeLoop = do
      -- cursorWindow <- gets rsCursorPos
      -- cursorCentered <- gets rsCursorP


      (key, _sink) <- Events.spawn
        handleEvent
        [ Key.callback
        -- , CursorPos.callback cursorWindow
        -- , MouseButton.callback cursorCentered MouseButton.clickHandler
        ]

      -- modify' \rs -> rs
      --   { rsEvents = Just sink
      --   }

      -- ImGui.beforeLoop True
      pure key

    afterLoop key = do
      -- ImGui.afterLoop
      Resource.release key

initialRunState :: Assets -> StageRIO env (Resource.ReleaseKey, RunState)
initialRunState rsAssets = do
  rsProjectionP <- Engine.getScreenP
  rsCursorPos <- Worker.newVar 0
  (cursorKey, rsCursorP) <- Worker.registered $
    Worker.spawnMerge2
      (\Camera.ProjectionInput{projectionScreen} (WithVec2 windowX windowY) ->
        let
          Vk.Extent2D{width, height} = projectionScreen
        in
          vec2
            (windowX - fromIntegral width / 2)
            (windowY - fromIntegral height / 2)
      )
      (Worker.getInput rsProjectionP)
      rsCursorPos

  (screenKey, screenBoxP) <- Worker.registered Layout.trackScreen

  (cameraKey, rsCameraP) <- Worker.registered Scene.spawnCamera

  (sceneKey, rsSceneP) <- Worker.registered $
    Worker.spawnMerge2 Scene.mkScene rsProjectionP rsCameraP

  (sceneUiKey, rsSceneUiP) <- Worker.registered $
    Worker.spawnMerge1 Scene.mkSceneUi rsProjectionP

  (uiKey, rsUI) <- UI.spawn rsAssets screenBoxP

  (worldKey, rsWorldSim) <- Worker.registered $
    WorldSystem.spawn rsAssets () -- (UI.playerOrbiter rsUI)

  -- (updateShadowKey, rsUpdateShadowP) <- Worker.registered $ Worker.spawnMerge2 (\_env _sun -> ()) rsEnvP rsSunP
  rsUpdateShadowP <- Worker.newVar ()
  rsUpdateShadow <- Worker.newObserverIO ()

  releaseKeys <- Resource.register $
    traverse_ @[] Resource.release
      [ cursorKey
      , screenKey
      , cameraKey
      , sceneKey
      , sceneUiKey
      , uiKey
      , worldKey
      -- , assetsKey
      ]

  -- let rsEvents = Nothing
  pure (releaseKeys, RunState{..})

initialFrameResources
  :: Assets
  -> Queues Vk.CommandPool
  -> RenderPasses
  -> Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
initialFrameResources assets _pools passes pipelines = do
  context <- ask

  (_lightsKey, lightsData) <- Buffer.allocateCoherent
      context
      Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT
      0
      Scene.staticLights

  frScene <- Set0.allocate
    (Render.getSceneLayout pipelines)
    (Assets.aTextures assets)
    (Assets.aCubeMaps assets)
    (Just lightsData)
    [ Image.aiImageView . ShadowPass.smDepthImage $
        Basic.rpShadowPass (Render.rpBasic passes)
    ]
    Nothing

  frSceneUi <- Set0.allocate
    (Render.getSceneLayout pipelines)
    (Assets.aTextures assets)
    (Assets.aCubeMaps assets)
    Nothing
    mempty
    Nothing

  -- TODO: extract to Sun.allocateStatic
  (frSunDescs, frSunData) <- Sun.createSet0Ds $
    Basic.getSunLayout (Render.pBasic pipelines)
  Buffer.updateCoherent Scene.staticLights frSunData

  ui <- gets rsUI
  frUI <- UI.newObserver ui

  frWorld <- World.newObserver
  let frDummy = ()

  pure FrameResources{..}

{-# LANGUAGE OverloadedLists #-}

{-# OPTIONS -Wwarn #-}

module Stage.Main.Render.Scene
  ( prepareCasters
  , prepareScene
  ) where

import RIO.Local

import RIO.Vector qualified as Vector
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.DescSets (Bound, Compatible)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Set0 qualified as Set0
import Render.Draw qualified as Draw

import Global.Resource.Assets qualified as Assets
import Global.Resource.Model qualified as Model
import Global.Render (StageFrameRIO, Pipelines(..))
import Stage.Main.Types (FrameResources(..), RunState(..))
import Stage.Main.UI qualified as UI
import Stage.Main.World qualified as World

type DrawM = StageFrameRIO FrameResources RunState

type DrawCasters dsl m = Vk.CommandBuffer -> Bound dsl () Transform m ()

type DrawData dsl m =
  ( Vk.CommandBuffer -> Bound dsl Void Void m ()
  , Vk.CommandBuffer -> Bound dsl Void Void m ()
  )

prepareCasters
  :: ( -- Compatible '[Sun] dsl
     )
  => FrameResources
  -> DrawM (DrawCasters dsl DrawM)
prepareCasters FrameResources{} = do
  -- collections <- gets rsObjectCollections

  -- tileSets <- traverse (traverse Worker.readObservedIO) frTiles

  let
    drawCasters _cb =
      pure ()
      -- for_ (Vector.zip collections tileSets) \(collection, tiles) ->
      --   flip Vector.imapM_ tiles \tileIx tileInstances ->
      --     case collection Vector.!? tileIx of
      --       Nothing ->
      --         pure ()
      --       Just found ->
      --         Draw.indexedPos cb (Object.model found) tileInstances

  pure drawCasters

prepareScene
  :: ( Compatible '[Scene] dsl
     )
  => Pipelines
  -> FrameResources
  -> DrawM (DrawData dsl DrawM)
prepareScene Pipelines{..} FrameResources{..} = do
  -- collections <- gets rsObjectCollections
  -- cubeModel <- gets (fst . rsUnitCube)

  -- gridLines <- traverse Worker.readObservedIO frGridLines
  -- (debugLinesModel, debugLinesInstance) <- Worker.readObservedIO frDebugLines

  -- cursor3d <- Worker.readObservedIO frCursor3d
  -- (cursorGrid, cursorGridInstance) <- Worker.readObservedIO frCursorGrid

  -- tileSets <- traverse (traverse Worker.readObservedIO) frTiles

  -- selectedObject <- gets rsSelectedObject
  -- (_origin, _transform, mobject) <- Worker.getOutputData selectedObject
  -- selectedObjectInstance <- Worker.readObservedIO frSelectedObject

  -- let (sunBox, _lights) = Scene.staticLights
  -- context <- asks fst
  -- (_, sunBoxInstance) <- Buffer.allocateCoherent context Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 1 [sunBox]

  Model.Collection{..} <- gets $ Assets.aModels . rsAssets
  World.WorldBuffers{..} <- World.readObserved frWorld

  let
    drawOpaque cb = do
      Set0.withBoundSet0 frScene (Basic.pLitColored pBasic) cb do
        Pipeline.bind cb (Basic.pLitColored pBasic) do
          -- TODO: use model from bodies
          Draw.indexed cb icosphere4 bodies

      -- Pipeline.bind cb pLitColored do
      --   for_ (Vector.zip collections tileSets) \(collection, tiles) ->
      --     flip Vector.imapM_ tiles \tileIx tileInstances ->
      --       case collection Vector.!? tileIx of
      --         Nothing ->
      --           pure ()
      --         Just found ->
      --           Draw.indexed cb (Object.model found) tileInstances

      --   -- for_ mobject \selected ->
      --   --   Draw.indexed cb (Object.model selected) selectedObjectInstance -- cursor3d

        Pipeline.bind cb (Basic.pWireframe pBasic) do
          Draw.indexed cb debugWires zeroTransform

    drawBlended _cb = do
      pure ()

  pure (drawOpaque, drawBlended)

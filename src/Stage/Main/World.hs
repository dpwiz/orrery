{-# LANGUAGE QuasiQuotes #-}

module Stage.Main.World where

import RIO.Local

import Apecs.STM.Prelude qualified as Apecs
import Control.Monad.Trans.Resource (ResourceT)
-- import Data.List (cycle)
import Engine.Types qualified as Engine
import Engine.Vulkan.Types (MonadVulkan)
import Engine.Worker qualified as Worker
-- import Geomancy.Transform qualified as Transform
-- import Geomancy.Vec3 qualified as Vec3
-- import Geomancy.Vec4 qualified as Vec4
import GHC.Clock (getMonotonicTimeNSec)
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model
import RIO.Vector.Storable qualified as Storable
import UnliftIO.Concurrent (forkIO)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Stage.Main.World.Body qualified as Body
import Stage.Main.World.Type (World)
import Stage.Main.World.Wire qualified as Wire

-- | Space scale to fit kilometers into engine depth range
pattern SCALE :: (Eq a, Floating a) => a
pattern SCALE = 0.00390625 -- 1/256

type Simulation = Simulation_ World

data Simulation_ world = Simulation
  { sWorker :: ThreadId
  , sOutput :: Worker.Var ()
  , sActive :: TVar Bool
  , sWorld  :: world
  }

instance Worker.HasWorker (Simulation_ world) where
  getWorker = sWorker

instance Worker.HasOutput (Simulation_ world) where
  type GetOutput (Simulation_ world) = ()
  getOutput Simulation{..} = sOutput

spawnSimulation
  :: MonadUnliftIO m
  => Int
  -> Bool
  -> m world
  -> (world -> m ())
  -> m (Simulation_ world)
spawnSimulation interval startActive initAction stepAction = do
  sActive <- newTVarIO startActive
  sOutput <- Worker.newVar ()

  sWorld <- initAction

  sWorker <- forkIO $ do
    startClock <- liftIO getMonotonicTimeNSec
    step sActive sWorld sOutput startClock
  pure Simulation{..}
  where
    step activeVar world output prevClock = do
      active <- readTVarIO activeVar
      when active do
        stepAction world
        Worker.updateOutput output Just

      stepClock <- liftIO getMonotonicTimeNSec
      let
        stepTime = (stepClock - prevClock) `div` 1000
        sleepTime = interval - fromIntegral stepTime
      threadDelay sleepTime

      -- sleepClock <- liftIO getMonotonicTimeNSec
      -- let sleepTimeActual = fromIntegral (sleepClock - stepClock) `div` 1000
      -- traceShowM
      --   ( interval
      --   , stepTime

      --   , sleepTime
      --   , sleepTimeActual
      --   , sleepTimeActual - sleepTime
      --   )

      nextClock <- liftIO getMonotonicTimeNSec
      step activeVar world output nextClock

{-

type DT = Quantity [si| s |] Double

data UIProbe = UIProbe
  { uiPlayerOrientation :: Worker.Var (Maybe (Orbiter.Player, Orbiter.Orientation, Maybe Orbiter.Thrust))
  , uiPlayerOrbit       :: Worker.Var (Maybe Orbiter)
  , uiTarget            :: Worker.Var (Maybe (Orbiter.Target, Orbiter))
  , uiMessageLog        :: [Worker.Var (Float, Vec4, Text)]
  }

mkTrajectory :: Orbit.Orbit Double -> Orbiter.Trajectory
mkTrajectory orbit =
  Orbiter.Trajectory do
    (a, b) <- zip vertices (drop 1 $ cycle vertices)
    [a, b]
  where
    velPeriapsis =
      velocityKmS . stateVectorsAtTrueAnomaly orbit $
        quOf 0 [si| rad |]

    velApoapsis =
      velocityKmS . stateVectorsAtTrueAnomaly orbit $
        turn |/| 2

    segments =
      case Orbit.period orbit of
        Nothing ->
          128 -- XXX: give a bit of leeway for non-elliptic orbits
        Just seconds ->
          max 64 . min 256 $ ceiling (seconds `numIn` [si| s |]) `div` 180

    segment = τ / fromInteger segments
    vertices = do
      i <- [0 .. segments]
      let
        atSegment =
          stateVectorsAtTrueAnomaly orbit $
            quOf (fromInteger i * segment) [si| rad |]

        position =
          toPosition $ StateVectors.position atSegment

        color =
          case Orbit.periapsisSpecifier orbit of
            Orbit.Circular ->
              circColor
            _ ->
              Vec4.lerp velocityNorm slowColor fastColor
              where
                velocityNorm = double2Float . max 0 . min 1 $
                  (velocityKmS atSegment - velApoapsis) /
                  (velPeriapsis - velApoapsis)

        slowColor = vec4 1 0 0 0.1
        fastColor = vec4 0 1 0 0.9
        circColor = vec4 0 0 1 0.9

      pure Model.Vertex
        { vPosition = position
        , vAttrs    = color
        }

velocityKmS :: StateVectors.StateVectors Double -> Double
velocityKmS =
  Linear.norm . fmap (`numIn` [si| km/s |]) . StateVectors.velocity

toPosition :: Linear.V3 (Distance Double) -> Vec3.Packed
toPosition v3 = Vec3.Packed $ toVec3 v3km Vec3.^* SCALE
  where
    v3km = fmap (double2Float . (`numIn` [si| km |])) v3

orbiterScale :: Transform
orbiterScale = Transform.scale (SCALE * 6371 / 32)

toVec3 :: Linear.V3 Float -> Vec3
toVec3 (Linear.V3 x y z) = vec3 x (-z) y

-- bodyRadius :: Quantity [si| km |] Double
-- bodyRadius = quOf 6371 [si| km |]

-- karmanLine :: Quantity [si| km |] Double
-- karmanLine = quOf 100 [si| km |]

-- bodyMass :: Quantity [si| kg |] Double
-- bodyMass = quOf 5.9722e24 [si| kg |]

-- bodyMu :: Quantity [si| m^3 s^-2 |] Double
-- bodyMu = bodyMass |*| gravity_G
-}

type Observer = Worker.ObserverIO WorldBuffers

data WorldBuffers = WorldBuffers
  { bodies     :: TransformBuffer
  , debugWires :: Wire.Model
  }

type TransformBuffer = Buffer.Allocated 'Buffer.Coherent Transform

newObserver :: ResourceT (Engine.StageRIO st) Observer
newObserver = do
  context <- ask

  bodies <- new 128

  debugWires <- Model.createCoherentEmpty context (2 * 128)
  void $! Resource.register $
    Model.destroyIndexed context debugWires

  Worker.newObserverIO WorldBuffers{..}

  where
    new size = do
      context <- ask

      (_transient, initialData) <-
        Buffer.allocateCoherent
          context
          Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
          size
          mempty

      pure initialData

collect
  :: forall components w m a
  .  ( Apecs.Get w m components
     , Apecs.Members w m components
     )
  => (components -> a)
  -> Apecs.SystemT w m [a]
collect f = Apecs.cfold (\acc comps -> f comps : acc) []

{-# INLINE observe #-}
observe :: MonadVulkan env m => Simulation -> Observer -> m ()
observe simP sim = do
  Worker.observeIO_ simP sim \WorldBuffers{..} () -> do
    (worldBodies, worldWires) <-
      atomically . Apecs.runWith (sWorld simP) $ (,)
        <$> collect Body.inst
        <*> collect (\(Wire.Wire vs) -> vs)

    context <- ask

    WorldBuffers
      <$> Buffer.updateCoherentResize_ context bodies (Storable.fromList worldBodies)
      <*> Model.updateCoherent context (concat worldWires) debugWires

{-# INLINE readObserved #-}
readObserved :: MonadUnliftIO m => Observer -> m WorldBuffers
readObserved = Worker.readObservedIO

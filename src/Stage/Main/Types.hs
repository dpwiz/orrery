module Stage.Main.Types where

import RIO

import Data.Tagged (Tagged)
import Geomancy (Vec2)
-- import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
-- import Engine.Worker (ObserverIO)
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 qualified as Set0
import Render.DescSets.Sun (Sun)
import Resource.Buffer qualified as Buffer

import Global.Render qualified as Render
-- import Global.Render.EnvCube.DescSets (Mapper)
import Global.Resource.Assets (Assets)
import Stage.Main.UI (UI)
import Stage.Main.UI qualified as UI
-- import Stage.Main.World.Env (Env)
-- import Stage.Main.World.Env qualified as Env
import Stage.Main.Scene qualified as Scene
import Stage.Main.World qualified as World
-- import Stage.Main.World.Sun qualified as Sun

type Stage = Render.Stage FrameResources RunState
type Frame = Render.Frame FrameResources

data FrameResources = FrameResources
  { frDummy :: ()

  , frScene         :: Set0.FrameResource '[Set0.Scene]
  , frSceneUi       :: Set0.FrameResource '[Set0.Scene]
  -- , frEnvCube       :: Set0.FrameResource '[Set0.Scene, Env, Mapper]
  -- , frEnvIrradiance :: Set0.FrameResource '[Set0.Scene, Env, Mapper]

  , frSunDescs :: Tagged '[Sun] (Vector Vk.DescriptorSet)
  , frSunData  :: Buffer.Allocated 'Buffer.Coherent Sun

  , frUI :: UI.Observer

  , frWorld :: World.Observer
  }

data RunState = RunState
  { rsProjectionP :: Camera.ProjectionProcess
  -- , rsViewP       :: Camera.ViewProcess
  , rsCameraP     :: Scene.CameraProcess

  , rsCursorPos :: Worker.Var Vec2
  , rsCursorP   :: Worker.Merge Vec2

  , rsSceneP :: Scene.Process
  -- , rsSceneV :: Scene.InputVar

  , rsSceneUiP :: Scene.Process

  , rsUpdateShadowP :: Worker.Var ()
  , rsUpdateShadow  :: Worker.ObserverIO ()

  , rsUI :: UI
  , rsWorldSim :: World.Simulation

  , rsAssets :: Assets
  }
